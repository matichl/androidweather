package com.example.mati.weather;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Intent simpleView = new Intent(getBaseContext(),WeatherActivity.class);

                Bundle bundle = new Bundle();
                String selected = (String) listAdapter.getChild(groupPosition, childPosition);
                bundle.putString("stuff", selected);
                simpleView.putExtras(bundle);
                startActivity(simpleView);

                return true;
            }
        });
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("POLSKA");
        listDataHeader.add("NIEMCY");
        listDataHeader.add("WIELKA BRYTANIA");

        // Adding child data
        List<String> poland = new ArrayList<String>();
        poland.add("Sieradz");
        poland.add("Warta");
        poland.add("Lodz");

        List<String> germany = new ArrayList<String>();
        germany.add("Berlin");
        germany.add("Bonn");
        germany.add("Studtgard");

        List<String> uk = new ArrayList<String>();
        uk.add("Londyn");
        uk.add("Liverpool");
        uk.add("West Bromich");

        listDataChild.put(listDataHeader.get(0), poland); // Header, Child data
        listDataChild.put(listDataHeader.get(1), germany);
        listDataChild.put(listDataHeader.get(2), uk);
    }
}